# Gruvbox colorscheme for your LaTeX documents

Use your favorite colorscheme to theme your entire document with this package.
```tex
\usepackage[dark]{tomorrow-colorscheme}
```

It define the following colors and their names (light):

- background: `#ffffff`
- font: `#4d4d4c`
- red: `#c82829`
- green: `#718c00`
- blue: `#4271ae`
- yellow: `#eab700`
- purple: `#8959a8`
- aqua: `#3e999f`
- gray: `#8e908c`
- orange: `#f5871f`

And with the `dark` option enabled:

- background: `#1d1f21`
- font: `#c5c8c6`
- red: `#cc6666`
- green: `#b5bd68`
- blue: `#81a2be`
- yellow: `#f0c674`
- purple: `#b294bb`
- aqua: `#8abeb7`
- gray: `#969896`
- orange: `#de935f`
